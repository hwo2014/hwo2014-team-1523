#include <errno.h>
#include <netdb.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include "cJSON.h"

#define TRACKPIECE_TYPE_STRAIGHT 0
#define TRACKPIECE_TYPE_BEND 1

#define MAX_TRACK_PIECES 128

#define FAST_PI 3.14
#define DEG_TO_RAD(X) (X*FAST_PI/180.0) 

/*0 straight, 1 bend int lengthOfTrackPiece;*/
typedef struct
{
	int typeOfTrackPiece;
	int lengthOfStraight;
	float angle;
	float radius;
} TrackPieceStruct;

typedef struct 
{
	char * trackName;
	char * trackID;
	int numberOfPieces;
} TheTrackDataStruct;

static cJSON *ping_msg();
static cJSON *turbo_msg();
static cJSON *join_msg(char *bot_name, char *bot_key);
static cJSON *throttle_msg(double throttle);
static cJSON *make_msg(char *type, cJSON *msg);

static cJSON *read_msg(int fd);
static void write_msg(int fd, cJSON *msg);

static void error(char *fmt, ...);
static int connect_to(char *hostname, char *port);
static void log_message(char *msg_type_name, cJSON *msg);

int main(int argc, char *argv[])
{
	TheTrackDataStruct * TheTrackData;
	TrackPieceStruct * TheTrackPieces;
    int sock;
	float currentThrottle, accumulatedSpeed, previousTravelDistance, previousTravelDistanceNoiseFilter, speedBasedOnDistanceDelta,  turboFactor;
	int turboAvailable, turboUsed, turboDurationTicks, isTurboActive;
    cJSON *json;
	char * nameOfTheBot;//be careful not to have a too long bot name!
	turboAvailable = 0;
	turboUsed = 0;
	isTurboActive = 0;
	previousTravelDistance = 0;
	previousTravelDistanceNoiseFilter = 0;
	speedBasedOnDistanceDelta = 0;
	turboFactor = 0.0f;
	turboDurationTicks = 0;
	if ( strlen(argv[3]) > 32 ) //no buffer overflows thanks.
		return 1;
	nameOfTheBot = malloc(strlen(argv[3])+1 * sizeof(char));
	sprintf(nameOfTheBot, "%s", argv[3]);

	currentThrottle = 1.0f;
	accumulatedSpeed = 0.0f;
	TheTrackPieces = malloc(MAX_TRACK_PIECES*sizeof(TrackPieceStruct));
	TheTrackData = malloc(sizeof(TheTrackData));

    if (argc != 5)
        error("Usage: bot host port botname botkey\n");

    sock = connect_to(argv[1], argv[2]);

    json = join_msg(argv[3], argv[4]);
    write_msg(sock, json);
    cJSON_Delete(json);
	/*BE_VERBOSEint msgCounter = 0;*/
    while ((json = read_msg(sock)) != NULL) {
        cJSON *msg, *msg_type;
        char *msg_type_name;

        msg_type = cJSON_GetObjectItem(json, "msgType");
        if (msg_type == NULL)
            error("missing msgType field");

        msg_type_name = msg_type->valuestring;
		/*printf("%d %s\n", msgCounter++, msg_type_name);*/
		printf("%s\n", msg_type_name);
		if (!strcmp("gameInit", msg_type_name)) {
			cJSON *theData, *theRace, *theTrack, *trackName, *trackID, *trackPieces;
			int i;
			i = 0;
			/*printf("gameInit received, doing magic.\n");*/
			theData = cJSON_GetObjectItem(json, "data");
			theRace = cJSON_GetObjectItem(theData, "race");
			theTrack = cJSON_GetObjectItem(theRace, "track");
			trackName = cJSON_GetObjectItem(theTrack, "name");
			trackID = cJSON_GetObjectItem(theTrack, "id");
			trackPieces = cJSON_GetObjectItem(theTrack, "pieces");

			TheTrackData->trackName = malloc(strlen(trackName->valuestring)*sizeof(char));
			sprintf(TheTrackData->trackName, "%s", trackName->valuestring);
			TheTrackData->trackID = malloc(strlen(trackID->valuestring)*sizeof(char));
			sprintf(TheTrackData->trackID, "%s", trackID->valuestring);
			TheTrackData->numberOfPieces = (int)cJSON_GetArraySize(trackPieces);

			//Print basic track data:
			//BE_VERBOSE//printf("trackName: %s\n",TheTrackData.trackName);
			//BE_VERBOSE//printf("trackID: %s\n",TheTrackData.trackID);
			//BE_VERBOSE//printf("trackPieces: %d\n",TheTrackData.numberOfPieces);

			////Analyze the pieces
			//BE_VERBOSE//printf("Analyzing track data...\n");
			for ( i = 0; i < TheTrackData->numberOfPieces; i++ )
			{
				//BE_VERBOSE//printf("TRACKPIECE TYPE:%d\n",cJSON_GetArrayItem(trackPieces,i)->type); 
				//DEBUG_LOG//printf("%d\t%s\n",i, cJSON_Print(cJSON_GetArrayItem(trackPieces,i)));
				//the data is inside an object like this { "blabla": 123 }
				if ( cJSON_GetArrayItem(trackPieces,i)->type == cJSON_Object ) {
					if ( cJSON_GetObjectItem(cJSON_GetArrayItem(trackPieces,i), "length") == NULL ) {
						//BE_VERBOSE//printf("SAVED TRACKPIECE %d AS: TRACKPIECE_TYPE_BEND\n",i);
						TheTrackPieces[i].typeOfTrackPiece = TRACKPIECE_TYPE_BEND;
						TheTrackPieces[i].angle = cJSON_GetObjectItem(cJSON_GetArrayItem(trackPieces,i), "angle")->valuedouble;
						TheTrackPieces[i].radius = cJSON_GetObjectItem(cJSON_GetArrayItem(trackPieces,i), "radius")->valuedouble;
					} else {
						//BE_VERBOSE//printf("SAVED TRACKPIECE %d AS: TRACKPIECE_TYPE_STRAIGHT\n",i);
						TheTrackPieces[i].typeOfTrackPiece = TRACKPIECE_TYPE_STRAIGHT;
						TheTrackPieces[i].lengthOfStraight = cJSON_GetObjectItem(cJSON_GetArrayItem(trackPieces,i), "length")->valueint;
					}
				}

				//BE_VERBOSE//printf("%s\n", cJSON_Print(cJSON_GetArrayItem(trackPieces,i)));

			}
			
			//cJSON_Delete(theData);
	
		}
		
		if (!strcmp("turboStart", msg_type_name)) { isTurboActive = 1; msg = ping_msg(); }
		if (!strcmp("turboEnd", msg_type_name)) { isTurboActive = 0; msg = ping_msg(); }
		if (!strcmp("turboAvailable", msg_type_name))
		{
			cJSON * temp;
			printf("TURBO AVAILABLE!");
			turboAvailable = 1;
			turboUsed = 0;
			temp = cJSON_GetObjectItem(json, "data");
			turboDurationTicks = cJSON_GetObjectItem(temp, "turboDurationTicks")->valueint;
			turboFactor = cJSON_GetObjectItem(temp, "turboFactor")->valuedouble;
		}

		if (!strcmp("carPositions", msg_type_name))
		{
			//printf("CORE DATA OBJECT TYPE:%d\n", cJSON_GetObjectItem(json, "data")->type);
			//BE_VERBOSE//printf("%s\n",  cJSON_Print(cJSON_GetObjectItem(json, "data")));

			{
				int batman;
				for ( batman = 0; batman < cJSON_GetArraySize(cJSON_GetObjectItem(json, "data")); batman++ )
				{
					//if ( )
					if ( cJSON_GetArrayItem(cJSON_GetObjectItem(json, "data"),batman)->type == cJSON_Object )
					{
						if ( !strcmp(cJSON_GetObjectItem(
										cJSON_GetObjectItem(
											cJSON_GetArrayItem(
												cJSON_GetObjectItem(json, "data"),batman),"id"), "name")->valuestring, nameOfTheBot) )
						{
							cJSON * coreData = cJSON_Duplicate(cJSON_GetArrayItem(cJSON_GetObjectItem(json, "data"),batman),1);
							cJSON * piecePositionItem = cJSON_Duplicate(cJSON_GetObjectItem(coreData,"piecePosition"),1);
							float slipAngle = fabs(cJSON_GetObjectItem(coreData, "angle")->valuedouble);
							batman = cJSON_GetObjectItem(piecePositionItem, "pieceIndex")->valueint;

							//printf("batman! :%d\n", batman);
						
							//BE_VERBOSE//printf("found the bot! At piece: %d",batman );
							//BE_VERBOSE//printf("TheTrackPieces->typeOfTrackPiece at BATMAN: %d\n", TheTrackPieces[batman].typeOfTrackPiece);
							if ( TheTrackPieces[batman].typeOfTrackPiece == TRACKPIECE_TYPE_BEND )
							{

								float distanceFromEndOfPiece = cJSON_GetObjectItem(piecePositionItem, "inPieceDistance")->valuedouble;
								float lengthOfBend = fabs( ( TheTrackPieces[batman].radius * DEG_TO_RAD(TheTrackPieces[batman].angle) ) );
								float percentageTraveled = distanceFromEndOfPiece / lengthOfBend;

								if ( distanceFromEndOfPiece == 0 ) //start of a new piece
									previousTravelDistance = 0;

								speedBasedOnDistanceDelta = fabs(previousTravelDistance - distanceFromEndOfPiece);
								if ( speedBasedOnDistanceDelta > 50.0f ) 
								{
									speedBasedOnDistanceDelta = fabs(previousTravelDistanceNoiseFilter - previousTravelDistance);
									printf("NOISE FILTER: DELTA DISTANCE ADJUSTED TO HISTORY!: %f\n\n",fabs(previousTravelDistanceNoiseFilter - previousTravelDistance));									
								}

								previousTravelDistanceNoiseFilter = previousTravelDistance;
								previousTravelDistance = distanceFromEndOfPiece;
								
								//LATEST_DEBUG//printf("BEND distanceFromEndOfPiece: %.1f\t", distanceFromEndOfPiece);
								//LATEST_DEBUG//printf("trackPiece length: %.1f\t", lengthOfBend);
								//LATEST_DEBUG//printf("distance traveled: %.1f\n", percentageTraveled );
								//LATEST_DEBUG//
								printf("TOTAL SPEED: %.3f\t", speedBasedOnDistanceDelta);
								//LATEST_DEBUG//
								printf("slipAngle: %.1f\n", slipAngle);
								
								float funnyThrottle = 1.0f - ((slipAngle+0.001f) * (speedBasedOnDistanceDelta*0.0039));
								if ( funnyThrottle < 0.0f ) funnyThrottle = 0.0001f;
								if ( funnyThrottle > 1.0f ) funnyThrottle = 1.0f;

								printf("1.0f - ((slipAngle+0.001f) * (speedBasedOnDistanceDelta*0.0039)): %.4f\n",funnyThrottle);
								currentThrottle = funnyThrottle;
							}
							else
							{
								float distanceFromEndOfPiece = cJSON_GetObjectItem(piecePositionItem, "inPieceDistance")->valuedouble;
								float lengthOfStraight = (float)TheTrackPieces[batman].lengthOfStraight;
								float percentageTraveled = distanceFromEndOfPiece / lengthOfStraight;
								if ( distanceFromEndOfPiece == 0 ) //start of a new piece
									previousTravelDistance = 0;
								speedBasedOnDistanceDelta = fabs(previousTravelDistance - distanceFromEndOfPiece);
								if ( speedBasedOnDistanceDelta > 50.0f ) 
								{
									speedBasedOnDistanceDelta = fabs(previousTravelDistanceNoiseFilter - previousTravelDistance);
									printf("NOISE FILTER: DELTA DISTANCE ADJUSTED TO HISTORY!: %f\n\n",fabs(previousTravelDistanceNoiseFilter - previousTravelDistance));		
								}

								previousTravelDistanceNoiseFilter = previousTravelDistance;
								previousTravelDistance = distanceFromEndOfPiece;
								//LATEST_DEBUG//printf("STRAIGHT distanceFromEndOfPiece: %.1f\t", distanceFromEndOfPiece);
								//LATEST_DEBUG//printf("trackPiece length: %.1f\t", lengthOfStraight);
								//LATEST_DEBUG//printf("distance traveled: %.1f\n", percentageTraveled);
								//LATEST_DEBUG//
								printf("TOTAL SPEED: %.3f\t", speedBasedOnDistanceDelta);
								//LATEST_DEBUG//
								printf("slipAngle: %.1f\n", slipAngle);
								printf("STRAIGHTSPEED 1.0f - (slipAngle+0.001f) * (speedBasedOnDistanceDelta*0.0038)): %.4f\n",1.0f - ((slipAngle+0.001f) * (speedBasedOnDistanceDelta*0.0038)));
								//printf("interpreted speed: %f", 

								if ( turboAvailable == 1 ) 
								{
									//if next two pieces are straight, use turbo
									if ( batman+3 == TheTrackData->numberOfPieces-3)
									{
										if ( TheTrackPieces[0].typeOfTrackPiece == TRACKPIECE_TYPE_STRAIGHT && TheTrackPieces[1].typeOfTrackPiece == TRACKPIECE_TYPE_STRAIGHT && TheTrackPieces[2].typeOfTrackPiece == TRACKPIECE_TYPE_STRAIGHT) {
											//SHUSH//printf("turboused!1");

											float roadLength = (float)TheTrackPieces[0].lengthOfStraight + TheTrackPieces[1].lengthOfStraight + TheTrackPieces[2].lengthOfStraight + (lengthOfStraight - distanceFromEndOfPiece);
											if ( turboDurationTicks * (turboFactor * speedBasedOnDistanceDelta) <= roadLength )
											{
												turboUsed = 1; 
											} else
												printf("NOT ENOUGH ROAD FOR TURBO!00 %f < %f\n",turboDurationTicks * (turboFactor * speedBasedOnDistanceDelta), roadLength);

										}

									} else if ( batman+2 == TheTrackData->numberOfPieces-2)
									{
										if ( TheTrackPieces[0].typeOfTrackPiece == TRACKPIECE_TYPE_STRAIGHT && TheTrackPieces[1].typeOfTrackPiece == TRACKPIECE_TYPE_STRAIGHT ) {
											//SHUSH//printf("turboused!1");

											float roadLength = (float)TheTrackPieces[0].lengthOfStraight + TheTrackPieces[1].lengthOfStraight + (lengthOfStraight - distanceFromEndOfPiece);
											if ( turboDurationTicks * (turboFactor * speedBasedOnDistanceDelta) <= roadLength )
											{
												turboUsed = 1; 
											} else
												printf("NOT ENOUGH ROAD FOR TURBO!11 %f < %f\n",turboDurationTicks * (turboFactor * speedBasedOnDistanceDelta), roadLength);
										
										}

									}
									else if ( batman+1 == TheTrackData->numberOfPieces-1 )
									{
										if ( TheTrackPieces[TheTrackData->numberOfPieces].typeOfTrackPiece == TRACKPIECE_TYPE_STRAIGHT && TheTrackPieces[0].typeOfTrackPiece == TRACKPIECE_TYPE_STRAIGHT ) {
											//SHUSH//printf("turboused!2");
											float roadLength = (float)TheTrackPieces[TheTrackData->numberOfPieces].lengthOfStraight + TheTrackPieces[0].lengthOfStraight + (lengthOfStraight - distanceFromEndOfPiece);
											if ( turboDurationTicks * (turboFactor * speedBasedOnDistanceDelta) <= roadLength+50.0f)
											{
												turboUsed = 1; 
											} else
												printf("NOT ENOUGH ROAD FOR TURBO!22 %f < %f\n",turboDurationTicks * (turboFactor * speedBasedOnDistanceDelta), roadLength);
										}

									} else if ( TheTrackPieces[batman+1].typeOfTrackPiece == TRACKPIECE_TYPE_STRAIGHT && TheTrackPieces[batman+2].typeOfTrackPiece == TRACKPIECE_TYPE_STRAIGHT )
									{
										//SHUSH//printf("turboused!3");
										float roadLength = (float)TheTrackPieces[batman+1].lengthOfStraight + TheTrackPieces[batman+2].lengthOfStraight + (lengthOfStraight - distanceFromEndOfPiece);
										if ( turboDurationTicks * (turboFactor * speedBasedOnDistanceDelta) <= roadLength+50.0f )
										{
											turboUsed = 1; 
										} else
										{
											printf("NOT ENOUGH ROAD FOR TURBO!33 %f < %f\n",turboDurationTicks * (turboFactor * speedBasedOnDistanceDelta), roadLength);
											if ( TheTrackPieces[batman+3].typeOfTrackPiece  == TRACKPIECE_TYPE_STRAIGHT )
											{
												printf("USING TURBO!!!!!!!!!!!!!11111111111232412312 !!!\n\n\n");
												turboUsed = 1;
											}
											else
												printf("TITTIES!!!!!!!\n\n\n");
											
										}

									} else {
										printf("Holding off turbo, no suitable 2 straights!");
									}

								}

								//if next piece is a bend, slow down when distance to end is close!
								if ( batman+1 >= TheTrackData->numberOfPieces ) batman = -1;
								if ( TheTrackPieces[batman+1].typeOfTrackPiece == TRACKPIECE_TYPE_BEND )
								{
									if ( speedBasedOnDistanceDelta > 6.7f )
										currentThrottle = 0.018f;
									else
										currentThrottle = 0.85f;
								}
								else
								{
									if (  speedBasedOnDistanceDelta > 8.0f )
										currentThrottle = 0.96f;
									else
										currentThrottle = 1.0f; 
								}

								if ( speedBasedOnDistanceDelta > 10.0f )
								{
									printf("\n\n\nTURBO FALLOFF!!!!\n\n\n");
									currentThrottle = 0.0f;

								}
								//DEBUG_LOG//printf("STRAIGHT DONE! SPD: %.3f\n", currentThrottle);
							}

							if ( currentThrottle > 0.9f )
							{
								accumulatedSpeed += 1.0f;
							}
							else
							{
								if ( accumulatedSpeed > 0.0f )
									accumulatedSpeed -= 1.0f;
								else
									accumulatedSpeed = 0.0f;
							}

							//LATEST_DEBUG//printf("SPEED: %f\n", accumulatedSpeed);

							if ( turboUsed == 1 ) 
							{
								turboAvailable = 0;
								turboUsed = 0;
								msg = turbo_msg();
							}
							else
								msg = throttle_msg(currentThrottle);

							cJSON_Delete(piecePositionItem);
							cJSON_Delete(coreData);
							break;
						}
					}
				}
				//for ( alfred = 0; alfred < 10; alfred++ )
				//	printf("tits: %d", alfred);
			}

			//piecePosition = cJSON_GetObjectItem(theData, "piecePosition");
			//pieceIndex = cJSON_GetObjectItem(piecePosition, "pieceIndex");
			//printf("pieceIndex: %d", pieceIndex->valueint);
           // msg = throttle_msg(0.65);//first test
			//printf("blaablaa");
        } else {
            log_message(msg_type_name, json);
            msg = ping_msg();
        }

        write_msg(sock, msg);

        cJSON_Delete(msg);
        cJSON_Delete(json);
    }

	memset(TheTrackPieces, 0, sizeof(TrackPieceStruct)*MAX_TRACK_PIECES);
	free(TheTrackPieces);
	free(TheTrackData);
	free(nameOfTheBot);

    return 0;
}

static cJSON *turbo_msg()
{
	return make_msg("turbo", cJSON_CreateString("wroom wroom turboboost oh yeah!"));
}

static cJSON *ping_msg()
{
    return make_msg("ping", cJSON_CreateString("ping"));
}

static cJSON *join_msg(char *bot_name, char *bot_key)
{
    cJSON *data = cJSON_CreateObject();
    cJSON_AddStringToObject(data, "name", bot_name);
    cJSON_AddStringToObject(data, "key", bot_key);

    return make_msg("join", data);
}

static cJSON *throttle_msg(double throttle)
{
    return make_msg("throttle", cJSON_CreateNumber(throttle));
}

static cJSON *make_msg(char *type, cJSON *data)
{
    cJSON *json = cJSON_CreateObject();
    cJSON_AddStringToObject(json, "msgType", type);
    cJSON_AddItemToObject(json, "data", data);
    return json;
}

static cJSON *read_msg(int fd)
{
    int bufsz, readsz;
    char *readp, *buf;
    cJSON *json = NULL;

    bufsz = 16;
    readsz = 0;
    readp = buf = malloc(bufsz * sizeof(char));

    while (read(fd, readp, 1) > 0) {
        if (*readp == '\n')
            break;

        readp++;
        if (++readsz == bufsz) {
            buf = realloc(buf, bufsz *= 2);
            readp = buf + readsz;
        }
    }

    if (readsz > 0) {
        *readp = '\0';
        json = cJSON_Parse(buf);
        if (json == NULL)
            error("malformed JSON(%s): %s", cJSON_GetErrorPtr(), buf);
    }
    free(buf);
    return json;
}

static void write_msg(int fd, cJSON *msg)
{
    char nl = '\n';
    char *msg_str;

    msg_str = cJSON_PrintUnformatted(msg);

    write(fd, msg_str, strlen(msg_str));
    write(fd, &nl, 1);

    free(msg_str);
}

static void error(char *fmt, ...)
{
	char buf[BUFSIZ];

	va_list ap;
	va_start(ap, fmt);
	vsnprintf(buf, BUFSIZ, fmt, ap);
	va_end(ap);

	if (errno)
		perror(buf);
	else
		fprintf(stderr, "%s\n", buf);

	exit(1);
}

static int connect_to(char *hostname, char *port)
{
	int status;
	int fd;
	char portstr[32];
	struct addrinfo hint;
	struct addrinfo *info;

	memset(&hint, 0, sizeof(struct addrinfo));
	hint.ai_family = PF_INET;
	hint.ai_socktype = SOCK_STREAM;

	sprintf(portstr, "%d", atoi(port));

	status = getaddrinfo(hostname, portstr, &hint, &info);
	if (status != 0) error("failed to get address: %s", gai_strerror(status));

	fd = socket(PF_INET, SOCK_STREAM, 0);
	if (fd < 0) error("failed to create socket");

	status = connect(fd, info->ai_addr, info->ai_addrlen);
	if (status < 0) error("failed to connect to server");

	freeaddrinfo(info);
	return fd;
}

static void log_message(char *msg_type_name, cJSON *msg)
{
	cJSON *msg_data;

	if (!strcmp("join", msg_type_name)) {
		puts("Joined");
	} else if (!strcmp("gameStart", msg_type_name)) {
		puts("Race started");
	} else if (!strcmp("crash", msg_type_name)) {
		puts("Someone crashed");
	} else if (!strcmp("gameEnd", msg_type_name)) {
		puts("Race ended");
	} else if (!strcmp("error", msg_type_name)) {
		msg_data = cJSON_GetObjectItem(msg, "data");
		if (msg_data == NULL)
			puts("Unknown error");
		else
			printf("ERROR: %s\n", msg_data->valuestring);
	}
}
#undef TRACKPIECE_TYPE_STRAIGHT
#undef TRACKPIECE_TYPE_BEND
#undef MAX_TRACK_PIECES
#undef FAST_PI